#include <NmraDcc.h>

NmraDcc  Dcc ;
DCC_MSG  Packet ;

#define DECODER_ADDRESS 24

void printBits(byte myByte){
  for(byte mask = 0x80; mask; mask >>= 1){
    if(mask  & myByte)
      Serial.print('1');
    else
      Serial.print('0');
  }
}

struct CVPair
{
  uint16_t  CV;
  uint8_t   Value;
};

CVPair FactoryDefaultCVs [] =
{
  {CV_ACCESSORY_DECODER_ADDRESS_LSB, 1},
  {CV_ACCESSORY_DECODER_ADDRESS_MSB, 0},

  // The CV Below defines the Short DCC Address
  {CV_MULTIFUNCTION_PRIMARY_ADDRESS, DECODER_ADDRESS},

  // These two CVs define the Long DCC Address
  {CV_MULTIFUNCTION_EXTENDED_ADDRESS_MSB, 0},
  {CV_MULTIFUNCTION_EXTENDED_ADDRESS_LSB, DECODER_ADDRESS},

};

uint8_t FactoryDefaultCVIndex = sizeof(FactoryDefaultCVs)/sizeof(CVPair);
void(* resetFunc) (void) = 0;  //declare reset function at address 0

void resetCVToDefault(){
  //Reset CV's to defaults. Power restart needed for changes to take effect
  Serial.println("[resetCVToDefault] CVs being reset to factory defaults");
  for (int j=0; j < FactoryDefaultCVIndex; j++ ){
    Serial.print("  CV");
    Serial.print(FactoryDefaultCVs[j].CV);
    Serial.print("=");
    Serial.println(FactoryDefaultCVs[j].Value);
    Dcc.setCV( FactoryDefaultCVs[j].CV, FactoryDefaultCVs[j].Value);
  }
};

// common callback functions

extern void notifyCVResetFactoryDefault() {
  //When anything is writen to CV8 reset to defaults. 
  Serial.println("[notifyCVResetFactoryDefault]");
  resetCVToDefault();  
  Serial.println("  Resetting...");
  delay(1000);  //typical CV programming sends the same command multiple times - specially since we dont ACK. so ignore them by delaying
  
  resetFunc();
};


extern void notifyDccReset(uint8_t hardReset ) {};
extern void notifyDccIdle(void) {};

extern void notifyDccMsg( DCC_MSG * Msg ) {
  int i;
  if( ( Msg->Data[0] == 0 ) && ( Msg->Data[1] == 0 ) ) return;  //reset packet
  if( ( Msg->Data[0] == 0b11111111 ) && ( Msg->Data[1] == 0 ) ) return;  //idle packet
  
  if(Msg->Data[0] == 100 && Msg->Data[1] == 63) return;
  
  Serial.print("[notifyDccMsg] ");
  Serial.print(Msg->Size);
  Serial.print(" ");
  for(i=0;i<Msg->Size;i++){
    //Serial.print(Msg->Data[i], BIN);
    printBits(Msg->Data[i]);
    Serial.print(" ");
  }
  
  Serial.println();
}

extern uint8_t notifyCVValid( uint16_t CV, uint8_t Writable ) {
  Serial.print("[notifyCVValid] CV");
  Serial.print(CV);
  Serial.print(" ");
  Serial.println(Writable);
};

extern uint8_t notifyCVRead( uint16_t CV) {
  if(CV != 29 && CV != 17 && CV != 18 && CV != 1){  //skip internally read CV's from output, the library reads CV29 and 1 very often.
     Serial.print("[notifyCVRead] CV");
     Serial.println(CV);
  }
  
  return eeprom_read_byte( (uint8_t*) CV ) ;
}

extern uint8_t notifyCVWrite( uint16_t CV, uint8_t Value) {
  Serial.print("[notifyCVWrite] CV");
  Serial.print(CV);
  Serial.print(" ");
  Serial.println(Value);
};

extern uint8_t notifyIsSetCVReady(void) {
  Serial.println("[notifyIsSetCVReady]");
};

extern void notifyCVChange( uint16_t CV, uint8_t Value) {
  Serial.print("[notifyCVChange] CV");
  Serial.print(CV);
  Serial.print(" ");
  Serial.println(Value);
};

extern void notifyDccCVChange( uint16_t CV, uint8_t Value) {
  Serial.print("[notifyDccCVChange] CV");
  Serial.print(CV);
  Serial.print(" ");
  Serial.println(Value);
};

extern void notifyCVAck(void) {
  Serial.println("[notifyCVAck]");
};

extern void notifyServiceMode(bool) {
  Serial.println("[notifyServiceMode] ");
  //Serial.println(bool);
};


// multi-function decoder callback functions

extern void notifyDccSpeed( uint16_t Addr, DCC_ADDR_TYPE AddrType, uint8_t Speed, DCC_DIRECTION Dir, DCC_SPEED_STEPS SpeedSteps ) {
  Serial.println("[notifyDccSpeed]");
};

extern void notifyDccSpeedRaw( uint16_t Addr, DCC_ADDR_TYPE AddrType, uint8_t Raw) {
  Serial.println("[notifyDccSpeedRaw]");
};

extern void notifyDccFunc( uint16_t Addr, DCC_ADDR_TYPE AddrType, FN_GROUP FuncGrp, uint8_t FuncState) {
  Serial.println("[notifyDccFunc]");
};


// accessory decoder callback functions

extern void notifyDccAccTurnoutBoard( uint16_t BoardAddr, uint8_t OutputPair, uint8_t Direction, uint8_t OutputPower ) {
   Serial.print("[notifyDccAccTurnoutBoard] ");

   Serial.print("BoardAddr: ");
   Serial.print(BoardAddr);   
   Serial.print(" OutputPair: ");
   Serial.print(OutputPair);   
   Serial.print(" Direction: ");
   Serial.print(Direction);   
   Serial.print(" OutputPower: ");
   Serial.print(OutputPower);   
  
   Serial.println();
}

extern void notifyDccAccTurnoutOutput( uint16_t Addr, uint8_t Direction, uint8_t OutputPower) {
   Serial.print("[notifyDccAccTurnoutBoard] ");

   Serial.print("Addr: ");
   Serial.print(Addr);   
   Serial.print(" Direction: ");
   Serial.print(Direction);   
   Serial.print(" OutputPower: ");
   Serial.print(OutputPower);   

   Serial.println();
}

extern void notifyDccAccBoardAddrSet( uint16_t BoardAddr) {
  Serial.println("[notifyDccAccBoardAddrSet]");
};

extern void notifyDccAccOutputAddrSet( uint16_t Addr) {
  Serial.println("[notifyDccAccOutputAddrSet]");
};

extern void notifyDccSigOutputState( uint16_t Addr, uint8_t State) {
  Serial.println("[notifyDccSigOutputState]");
};

extern void notifyDccAccState( uint16_t Addr, uint16_t BoardAddr, uint8_t OutputAddr, uint8_t State ) {
  // deprecated
  Serial.println("[notifyDccAccState]");
};

extern void notifyDccSigState( uint16_t Addr, uint8_t OutputIndex, uint8_t State) {
  // deprecated
  Serial.println("[notifyDccSigState]");
};


void setup() {
  uint8_t cv_value;
  Serial.begin(115200);
  
  // Setup which External Interrupt, the Pin it's associated with that we're using, disable pullup.
  Dcc.pin(0, 2, 0);
  
  // Call the main DCC Init function to enable the DCC Receiver
  Dcc.init( MAN_ID_DIY, 100, FLAGS_DCC_ACCESSORY_DECODER, 0 ); 
  //Dcc.init( MAN_ID_DIY, 100, FLAGS_DCC_ACCESSORY_DECODER | FLAGS_OUTPUT_ADDRESS_MODE, 0 ); 

  resetCVToDefault();  

  Serial.println("Ready");     
}

void loop(){
  // You MUST call the NmraDcc.process() method frequently from the Arduino loop() function for correct library operation
  Dcc.process();
}
