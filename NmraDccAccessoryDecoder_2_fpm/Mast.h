#define MAX_PINS_PER_MAST 16
#define NUMBER_PINS_PER_MAST 4
#define ACTIVE_OUTPUT_STATE LOW      // Set the ACTIVE State of the output to Drive the Turnout motor electronics HIGH or LOW 
#define NUMBER_ASPECTS 32

void printBits(byte);

class Mast {
  private:
    uint16_t Addr;
    uint8_t State;
    uint16_t flashMs;
    unsigned long previousMillis;
    boolean flashState;
    byte mastPins[NUMBER_PINS_PER_MAST];
    uint8_t* aspectOutputs;
  public:
    void init(uint16_t);
    void setState(uint8_t);
    void setFlashMs(uint16_t);
    void setAspectOutputs(uint8_t*);
    void assignSetOfPins(byte*, uint8_t, uint8_t);
    void process(void);
};
