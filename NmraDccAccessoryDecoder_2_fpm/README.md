## NmraDccAccessoryDecoder_2_fpm

Accessory (static) decoder that uses notifyDccSigOutputState().

### TOC
- [Hardware](#hardware)
- [CV values](#cv-values)
- [Aspect definition](#aspect-definition)
- [Connections](#connections)
- [Usage with JMRI](#usage-with-jmri)

### Hardware

Based on an [Arduino](http://www.francescpinyol.cat/arduino.html) PRO Mini.

### CV values

|CV |define         |default value      |description                 |
|---|---------------|-------------------|----------------------------|
|1  |CV_ACCESSORY_DECODER_ADDRESS_LSB|DEFAULT_ACCESSORY_DECODER_ADDRESS & 0xff||
|9  |CV_ACCESSORY_DECODER_ADDRESS_MSB|DEFAULT_ACCESSORY_DECODER_ADDRESS >>8||
|112|CV_FLASH_CS    |DEFAULT_FLASH_CS `50`|flash time (in centiseconds)|
|140|CV_OUTPUTS_BASE|`0x20` 0010.0000 (solid red) (P, FF7A)                |aspect 0: absolute stop (NMRA S-9.2.1 2.4.2)|
|141|               |`0x80` 1000.0000 (solid green) (VL, FF1A)             |aspect 1 definition|
|142|               |`0x40` 0100.0000 (flash green) (VL', FF2)             |aspect 2 definition|
|143|               |`0x88` 1000.1000 (solid green, solid yellow) (PR, FF3)|aspect 3 definition|
|144|               |`0x08` 0000.1000 (solid yellow) (AP, FF5)             |aspect 4 definition|
|145|               |`0x04` 0000.0100 (flash yellow) (AP', FF6)            |aspect 5 definition|
|146|               |`0x22` 0010.0010 (solid red, solid white) (RA, FF8B)  |aspect 7 definition|
|147|               |`0x21` 0010.0001 (solid red, flash white) (RA', FF8A) |aspect 8 definition|
|148|               |`0x02` 0000.0010 (solid white) (MA, FF9)              |aspect 9 definition|
|149|               |`0x00`               |aspect 10 definition|
|...||||
|171|               |`0x00`               |aspect 31 definition: all lights off|

### Aspect definition

8 bits, each one: `<output><mode>`
where mode is: 
  - C: continuous
  - F: flash

1C 1F 2C 2F 3C 3F 4C 4F

Examples:
  - 0x80 1000.0000 means output 1 is continuous
  - 0x21 0010.0001 means output 2 is continuous; output 4 is flashing

### Connections

Common anode (positive).

When default values are set in CV141..CV149, connection of several 4-colour GRYW [ADIF masts](http://www.francescpinyol.cat/modelisme.html#senyalitzacio).

Mast index is internal value of variable `mastIndex`.

Pin label for an Arduino PRO Mini.

|accessory address|mast|pin|colour|
|-----------------|----|---|------|
|1                |0   |3  |green |
|1                |0   |4  |red   |
|1                |0   |5  |yellow|
|1                |0   |6  |white |
|2                |1   |7  |green |
|2                |1   |8  |red   |
|2                |1   |9  |yellow|
|2                |1   |10 |white |
|3                |2   |11 |green |
|3                |2   |12 |red   |
|3                |2   |13 |yellow|
|3                |2   |A0 |white |
|4                |3   |A2 |green |
|4                |3   |A3 |red   |
|4                |3   |A4 |yellow|
|4                |3   |A5 |white |


### Usage with JMRI

In order to control the decoder from [JMRI](http://www.francescpinyol.cat/jmri.html) signal table in PanelPro:
1. install [jmri-signals](https://gitlab.com/francesc.pinyol.m/jmri-signals)

In order to program the decoder from [JMRI](http://www.francescpinyol.cat/jmri.html) DecoderPro (or Roster in PanelPro):
1. copy or link provided file [accessory_decoder_fpm.xml](accessory_decoder_fpm.xml) to .../JMRI/xml/decoders/
2. rebuild decoder database (choose one of the following):
  - from PanelPro: Debug -> Recreate Decoder Index
  - from DecoderPro (or Roster): Actions -> Recreate Decoder Index
