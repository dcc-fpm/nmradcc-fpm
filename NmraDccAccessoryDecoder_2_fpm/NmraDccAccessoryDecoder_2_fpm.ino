#include <NmraDcc.h>
#include "Mast.h"

// you can also print every DCC packet by uncommenting the "#define NOTIFY_DCC_MSG" line below
#define NOTIFY_DCC_MSG

// You can also print other Debug Messages uncommenting the line below
#define DEBUG_MSG

// Un-Comment the line below to force CVs to be written to the Factory Default values
// defined in the FactoryDefaultCVs below on Start-Up
#define FORCE_RESET_FACTORY_DEFAULT_CV

// Un-Comment the line below to Enable DCC ACK for Service Mode Programming Read CV Capablilty 
#define ENABLE_DCC_ACK  15  // This is A1 on the Iowa Scaled Engineering ARD-DCCSHIELD DCC Shield

// Define the Arduino input Pin number for the DCC Signal 
#define DCC_PIN     2

#define NUMBER_MASTS 4
#define ACCESSORY_ADDRESS_OFFSET 0
#define CV_OUTPUTS_BASE 140  // first cv address to store output setup for 32 aspects

#define DCC_DECODER_VERSION_NUM 10  // Set the Decoder Version - Used by JMRI to Identify the decoder

struct CVPair
{
  uint16_t  CV;
  uint8_t   Value;
};

// first address in this decoder
//const uint16_t ACC_ADDR = 1; // 11 bit address:
//                             // 0 - broadcast, 1 to 2044 - specific.

// address for ops mode (needed? for POM?)
#define CV_OPS_MODE_ADDRESS_BASE   17
#define DEFAULT_OPS_MODE_ADDRESS       9999           // this address is for setting cv's like a loco

// flashing period 
#define CV_FLASH_CS 112
#define DEFAULT_FLASH_CS 50 // cs (centiseconds) for flashing lights

//void printBits(byte myByte){
//  for(byte mask = 0x80; mask; mask >>= 1){
//    if (mask==0x8)
//      Serial.print('.');
//    if(mask  & myByte)
//      Serial.print('1');
//    else
//      Serial.print('0');
//  }
//}


// To set the first accessory output addresse for this board you need to change the CV values for CV1 (CV_ACCESSORY_DECODER_ADDRESS_LSB) and 
// CV9 (CV_ACCESSORY_DECODER_ADDRESS_MSB) in the FactoryDefaultCVs structure below.

CVPair FactoryDefaultCVs [] =
{
  {CV_ACCESSORY_DECODER_ADDRESS_LSB, DEFAULT_ACCESSORY_DECODER_ADDRESS & 0xff},     // Accessory address LSB.
  {CV_ACCESSORY_DECODER_ADDRESS_MSB, DEFAULT_ACCESSORY_DECODER_ADDRESS >> 8},     // Accessory address MSB.
  {CV_OPS_MODE_ADDRESS_BASE, DEFAULT_OPS_MODE_ADDRESS & 0xFF },        // LSB Ops Mode Address
  {CV_OPS_MODE_ADDRESS_BASE+1,(DEFAULT_OPS_MODE_ADDRESS>>8) & 0x3F },  // MSB Ops Mode Address
  {CV_FLASH_CS, DEFAULT_FLASH_CS},
  {CV_OUTPUTS_BASE, 0x20}, // P / FF7A
  {CV_OUTPUTS_BASE+1, 0x80}, // VL / FF1A
  {CV_OUTPUTS_BASE+2, 0x40}, // VL' / FF2
  {CV_OUTPUTS_BASE+3, 0x88}, // PR / FF3
  {CV_OUTPUTS_BASE+4, 0x08}, // AP / FF5
  {CV_OUTPUTS_BASE+5, 0x04}, // AP' / FF6
  {CV_OUTPUTS_BASE+6, 0x22}, // RA / FF8B
  {CV_OUTPUTS_BASE+7, 0x21}, // RA' / FF8A
  {CV_OUTPUTS_BASE+8, 0x02}, // MA / FF9
};

uint8_t FactoryDefaultCVIndex = 0;


int tim_delay = 500;
#define numleds  16
byte ledpins[] = { 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 18, 19};
//   pins         D3 D4 D5 D6 D7 D8 D9 D10 D11 D12 D13  A0  A2  A3  A4  A5  


NmraDcc  Dcc ;
Mast masts[NUMBER_MASTS];
uint8_t globalAspectOutputs[NUMBER_ASPECTS];

void setGlobalAspectOutputs()
{
  // set outputs for each aspect
  for (int aspect=0; aspect<NUMBER_ASPECTS; aspect++) {
    Serial.print("[setGlobalAspectOutputs] setting internal variable globalAspectOutputs for aspect ");
    Serial.print(aspect);
    Serial.print(", by getting value from CV ");
    Serial.print(CV_OUTPUTS_BASE + aspect);
    Serial.print(": ");
    uint8_t value = Dcc.getCV(CV_OUTPUTS_BASE + aspect);
    printBits(value);
    Serial.println("");
    globalAspectOutputs[aspect] = value;
  }
}
void setup()
{
  Serial.begin(115200);
  
 // initialize the digital pins as outputs
  for (int i=0; i< numleds; i++) {
    pinMode(ledpins[i], OUTPUT);
    digitalWrite(ledpins[i], LOW);
   }
  for (int i=0; i< numleds; i++) {
     digitalWrite(ledpins[i], ACTIVE_OUTPUT_STATE);
     delay (tim_delay/10);
  }
  delay( tim_delay);
  for (int i=0; i< numleds; i++) {
     digitalWrite(ledpins[i], ! ACTIVE_OUTPUT_STATE);
     delay (tim_delay/10);
  }
  delay( tim_delay);

  // Setup which External Interrupt, the Pin it's associated with that we're using and enable the Pull-Up
  // Many Arduino Cores now support the digitalPinToInterrupt() function that makes it easier to figure out the
  // Interrupt Number for the Arduino Pin number, which reduces confusion. 
#ifdef digitalPinToInterrupt
  Dcc.pin(DCC_PIN, 0);
#else
  Dcc.pin(0, DCC_PIN, 1);
#endif
  
  Dcc.initAccessoryDecoder( MAN_ID_DIY, DCC_DECODER_VERSION_NUM, FLAGS_OUTPUT_ADDRESS_MODE, CV_OPS_MODE_ADDRESS_BASE );

#ifdef FORCE_RESET_FACTORY_DEFAULT_CV
  Serial.println("[setup] Resetting CVs to Factory Defaults");
  notifyCVResetFactoryDefault(); 
#endif

  // get first address from CV1,9 (CV_ACCESSORY_DECODER_ADDRESS_MSB, CV_ACCESSORY_DECODER_ADDRESS_LSB)
  uint16_t myFirstAccessoryAddress = Dcc.getAddr();
  Serial.print("[setup] myFirstAccessoryAddress: ");
  Serial.println(myFirstAccessoryAddress);

  // get ops mode address
  uint16_t FakeOpsAddr = Dcc.getCV( CV_OPS_MODE_ADDRESS_BASE ) | ( Dcc.getCV( CV_OPS_MODE_ADDRESS_BASE + 1 ) << 8 ) ;
  Serial.print("[setup] FakeOpsAddr: ");
  Serial.println(FakeOpsAddr);

  // init aspectOutputs
  setGlobalAspectOutputs();

  // init all masts
  for (int i=0; i< NUMBER_MASTS; i++) {
    Serial.print("[setup] -- initialising mast ");
    Serial.println(i);
    masts[i].init(i);
    masts[i].assignSetOfPins(ledpins, i*NUMBER_PINS_PER_MAST, NUMBER_PINS_PER_MAST);
    // all leds off
    masts[i].setState(0);
    masts[i].setFlashMs(Dcc.getCV(CV_FLASH_CS)*10);
    masts[i].setAspectOutputs(globalAspectOutputs);
  }    

  // set reset default values as pending

  printCvs();
}

void loop()
{
  // You MUST call the NmraDcc.process() method frequently from the Arduino loop() function for correct library operation
  //Serial.println("[loop] calling Dcc.process");
  Dcc.process();
  
  for (int i=0; i< NUMBER_MASTS; i++) {
    //Serial.print("[loop] calling mast.process for mast ");
    //Serial.println(i);
    masts[i].process();
  }

  // set pending default values
  if( FactoryDefaultCVIndex && Dcc.isSetCVReady())
  {
    FactoryDefaultCVIndex--; // Decrement first as initially it is the size of the array 
    Serial.print("[loop] Default value: CV");
    Serial.print(FactoryDefaultCVs[FactoryDefaultCVIndex].CV);
    Serial.print("=");
    Serial.println(FactoryDefaultCVs[FactoryDefaultCVIndex].Value);
    Dcc.setCV(  FactoryDefaultCVs[FactoryDefaultCVIndex].CV,
                FactoryDefaultCVs[FactoryDefaultCVIndex].Value);
  }
  
}

void notifyDccSigOutputState( uint16_t Addr, uint8_t State) {
  Serial.print("[notifyDccSigOutputState] ") ;
  Serial.print(Addr,DEC) ;
  Serial.print(',');
  Serial.println(State, HEX) ;

  // get ops mode address
  uint16_t FakeOpsAddr = Dcc.getCV( CV_OPS_MODE_ADDRESS_BASE ) | ( Dcc.getCV( CV_OPS_MODE_ADDRESS_BASE + 1 ) << 8 ) ;
  Serial.print("[notifyDccSigOutputState] FakeOpsAddr: ");
  Serial.println(FakeOpsAddr);
  
  
  // get first address from CV1,9 (CV_ACCESSORY_DECODER_ADDRESS_MSB, CV_ACCESSORY_DECODER_ADDRESS_LSB)
  uint16_t myFirstAccessoryAddress = Dcc.getAddr();
  Serial.print("[notifyDccSigOutputState] myFirstAccessoryAddress: ");
  Serial.println(myFirstAccessoryAddress);

  // get CV15
  uint8_t value = Dcc.getCV(15);
  Serial.print("[notifyDccSigOutputState] CV15=");
  Serial.println(value);

  // calculate mast index from address
  uint16_t accessoryAddress = Addr + ACCESSORY_ADDRESS_OFFSET;
  if ( accessoryAddress >= myFirstAccessoryAddress && accessoryAddress < myFirstAccessoryAddress + NUMBER_MASTS) {
    uint8_t mastIndex = Addr + ACCESSORY_ADDRESS_OFFSET - myFirstAccessoryAddress;
    Serial.print("[notifyDccSigOutputState] mastIndex: ");
    Serial.println(mastIndex,DEC);
    masts[mastIndex].setState(State);
  } else {
    Serial.print("[notifyDccSigOutputState] Received address ");
    Serial.print(accessoryAddress);
    Serial.println(" is not for this decoder");
  }
}

void notifyCVChange(uint16_t CV, uint8_t Value)
{
#ifdef DEBUG_MSG
  Serial.print("[notifyCVChange] CV: ") ;
  Serial.print(CV,DEC) ;
  Serial.print(" Value: ") ;
  Serial.println(Value, DEC) ;
#else
  Value = Value;  // Silence Compiler Warnings...
  CV = CV;
#endif  

  if (CV==CV_FLASH_CS){
    for (int i=0; i< NUMBER_MASTS; i++) {
      masts[i].setFlashMs(Value*10);
    }    
  }

  if (CV>=CV_OUTPUTS_BASE  && CV< CV_OUTPUTS_BASE + NUMBER_ASPECTS){
    setGlobalAspectOutputs();
    for (int i=0; i< NUMBER_MASTS; i++) {
      masts[i].setAspectOutputs(globalAspectOutputs);
    }    
  }

}

void notifyCVResetFactoryDefault()
{
  // all default cv values must be set (in loop)
  // Make FactoryDefaultCVIndex non-zero and equal to num CV's to be reset 
  // to flag to the loop() function that a reset to Factory Defaults needs to be done
  Serial.println(F("notifyCVResetFactoryDefault called."));
  FactoryDefaultCVIndex = sizeof(FactoryDefaultCVs)/sizeof(CVPair);
};

// This function is called by the NmraDcc library when a DCC ACK needs to be sent
// Calling this function should cause an increased 60ma current drain on the power supply for 6ms to ACK a CV Read 
#ifdef  ENABLE_DCC_ACK
void notifyCVAck(void)
{
#ifdef DEBUG_MSG
  Serial.println("[notifyCVAck]") ;
#endif
  // Configure the DCC CV Programing ACK pin for an output
  pinMode( ENABLE_DCC_ACK, OUTPUT );

  // Generate the DCC ACK 60mA pulse
  digitalWrite( ENABLE_DCC_ACK, HIGH );
  delay( 10 );  // The DCC Spec says 6ms but 10 makes sure... ;)
  digitalWrite( ENABLE_DCC_ACK, LOW );
}
#endif

#ifdef  NOTIFY_DCC_MSG
void notifyDccMsg( DCC_MSG * Msg)
{
  int i;
  if( ( Msg->Data[0] == 0 ) && ( Msg->Data[1] == 0 ) ) return;  //reset packet
  if( ( Msg->Data[0] == 0b11111111 ) && ( Msg->Data[1] == 0 ) ) return;  //idle packet
  
  if(Msg->Data[0] == 100 && Msg->Data[1] == 63) return;
  
  Serial.print("[notifyDccMsg] ");
  Serial.print(Msg->Size);
  Serial.print(" ");
  for(i=0;i<Msg->Size;i++){
    //Serial.print(Msg->Data[i], BIN);
    printBits(Msg->Data[i]);
    Serial.print(" ");
  }
  
  Serial.println();
}
#endif


void notifyServiceMode(bool inServiceMode)
{
  Serial.print("[notifyServiceMode] ");
  Serial.println(inServiceMode);
};

void printCvs()
{
  Serial.print("CV7: ");
  Serial.println(Dcc.getCV(7));

  Serial.print("CV8: ");
  Serial.println(Dcc.getCV(8));  
}
