#include <NmraDcc.h>
#include "Mast.h"

void printBits(byte myByte){
  for(byte mask = 0x80; mask; mask >>= 1){
    if (mask==0x8)
      Serial.print('.');
    if(mask  & myByte)
      Serial.print('1');
    else
      Serial.print('0');
  }
}

void Mast::init(uint16_t Addr) {
  Serial.println("Mast::init");
  this->Addr = Addr;
  flashMs = 500;
  previousMillis = millis();
}

void Mast::assignSetOfPins(byte *realPins, uint8_t startPin, uint8_t numberPins) {
  // assign a group of pins to this mast
  Serial.println("Mast::assignSetOfPins");
  for (uint8_t pinIndex=0; pinIndex<numberPins; pinIndex++) {
    Serial.print("   pinIndex ");
    Serial.print(pinIndex);
    Serial.print(" = real pin ");
    Serial.println(realPins[startPin+pinIndex]);
    this->mastPins[pinIndex] = realPins[startPin+pinIndex];
  }
}

void Mast::setState(uint8_t State) {
  // set the state for this mast
  Serial.print("Mast::setState ");
  Serial.println(State);
  this->State = State;
}

void Mast::setFlashMs(uint16_t FlashMs) {
  // set the state for this mast
  Serial.print("Mast::setFlashMs ");
  Serial.println(FlashMs);
  this->flashMs = FlashMs;
}

void Mast::setAspectOutputs(uint8_t* aspectOutputs) {
  // set the outputs for each aspec
  Serial.println("Mast::setAspectOutputs ");
  //Serial.println(*aspectOutputs);
  //printBits(*aspectOutputs);
  //Serial.println("");
  this->aspectOutputs = aspectOutputs;
}

void Mast::process() {
  // green
  // red
  // yellow
  // lunar

  boolean onState = ACTIVE_OUTPUT_STATE;
  boolean offState = ! ACTIVE_OUTPUT_STATE;
  boolean pinOut;

  //Serial.println("Mast::process");
  unsigned long currentMillis = millis();

  if (currentMillis - this->previousMillis >= this->flashMs) {
    //Serial.println("changing flashState");
    // save the last time you blinked the LED
    this->previousMillis = currentMillis;
    this->flashState = ! this->flashState;
  }

  uint8_t out = aspectOutputs[this->State];
//  Serial.print("Mast ");
//  Serial.print(this->Addr);
//  Serial.print(" ");
//  printBits(out);
//  Serial.println("");

  // 8 bits, each one: <output><mode>
  // where mode is: 
  // - C: continuous
  // - F: flash
  // 1C 1F 2C 2F 3C 3F 4C 4F
  // Examples:
  // - 0x80 1000.0000 means output 1 is continuous
  // - 0x21 0010.0001 means output 2 is continuous; output 4 is flashing
  
  for (int i=0; i<NUMBER_PINS_PER_MAST; i++) {
    // default is off
    pinOut = offState;

    // continuous on
    uint8_t outContinuous = (out >> (7-2*i)) & 0x01;
    //Serial.print(" continuous: ");
    //printBits(outContinuous);
    //Serial.println("");
    if (outContinuous==1){
      pinOut = onState;
    }
    else {
      // flash
      uint8_t outFlash = (out >> (7-2*i-1)) & 0x01;
      //Serial.print(" flash: ");
      //printBits(outContinuous);
      //Serial.println("");
      if (outFlash==1) {
        pinOut = this->flashState;
      }
    }
    
  //Serial.print("pin ");
  //Serial.print(mastPins[i]);
  //Serial.print(" set to ");
  //Serial.println(pinOut);

    // set pin to status
    digitalWrite(mastPins[i], pinOut);
  }

//  switch(this->State)
//  {    
//    case 0: // all off
//      digitalWrite(mastPins[0], offState);
//      digitalWrite(mastPins[1], offState);
//      digitalWrite(mastPins[2], offState);
//      digitalWrite(mastPins[3], offState);
//      break;    
//    case 1: // VL / FF1A
//      // green
//      digitalWrite(mastPins[0], onState);
//      digitalWrite(mastPins[1], offState);
//      digitalWrite(mastPins[2], offState);
//      digitalWrite(mastPins[3], offState);
//      break;
//    case 2: // VL' / FF2
//      // flashing green
//      digitalWrite(mastPins[0], flashState);
//      digitalWrite(mastPins[1], offState);
//      digitalWrite(mastPins[2], offState);
//      digitalWrite(mastPins[3], offState);
//      break;
//    case 3: // PR / FF3
//      // green
//      // yellow
//      digitalWrite(mastPins[0], onState);
//      digitalWrite(mastPins[1], offState);
//      digitalWrite(mastPins[2], onState);
//      digitalWrite(mastPins[3], offState);
//      break;
//    case 4: // AP /  FF5
//      // yellow
//      digitalWrite(mastPins[0], offState);
//      digitalWrite(mastPins[1], offState);
//      digitalWrite(mastPins[2], onState);
//      digitalWrite(mastPins[3], offState);
//      break;
//    case 5: // AP' / FF6
//      // flashing yellow
//      digitalWrite(mastPins[0], offState);
//      digitalWrite(mastPins[1], offState);
//      digitalWrite(mastPins[2], flashState);
//      digitalWrite(mastPins[3], offState);
//      break;
//    case 6: // P / FF7A
//      // red
//      digitalWrite(mastPins[0], offState);
//      digitalWrite(mastPins[1], onState);
//      digitalWrite(mastPins[2], offState);
//      digitalWrite(mastPins[3], offState);
//      break;
//    case 7: // RA / FF8B
//      // red
//      // lunar
//      digitalWrite(mastPins[0], offState);
//      digitalWrite(mastPins[1], onState);
//      digitalWrite(mastPins[2], offState);
//      digitalWrite(mastPins[3], onState);
//      break;
//    case 8: // RA' / FF8A
//      // red
//      // flash lunar
//      digitalWrite(mastPins[0], offState);
//      digitalWrite(mastPins[1], onState);
//      digitalWrite(mastPins[2], offState);
//      digitalWrite(mastPins[3], flashState);
//      break;
//    case 9: // MA / FF9
//      // lunar
//      digitalWrite(mastPins[0], offState);
//      digitalWrite(mastPins[1], offState);
//      digitalWrite(mastPins[2], offState);
//      digitalWrite(mastPins[3], onState);
//      break;
//  }
}
